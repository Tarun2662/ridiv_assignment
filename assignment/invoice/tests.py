# myapp/tests/test_views.py
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from .models import Invoice

class InvoiceAPITests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.invoice_data = {'date': '2001-01-18', 'customer_name': 'Appari Tarun'}
        self.invoice = Invoice.objects.create(**self.invoice_data)

    def test_create_invoice(self):
        url = reverse('invoicelistcreate')
        response = self.client.post(url, data=self.invoice_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Invoice.objects.count(), 2)

    def test_list_invoices(self):
        url = reverse('invoicelistcreate')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    
