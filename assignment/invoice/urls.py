from django.urls import path
from .views import InvoiceListCreateView, InvoiceDetailView,InvoiceDetail,InvoiceDetailListCreateView

urlpatterns = [
    path('invoices/', InvoiceListCreateView.as_view(), name='invoicelistcreate'),
    path('invoices/<int:pk>/', InvoiceDetailView.as_view(), name='invoicedetail'),
    path('invoicesDetail/', InvoiceDetailListCreateView.as_view(), name='invoicedetail'),
]
